from Constants import LOG_PATH


def info(text):
    message = 'INFO: {}\n'.format(text)

    log_file = open(LOG_PATH, 'a+')
    log_file.write(message)
    log_file.close()

    print(message)


def error(error):
    message = '{splitter}\nERROR: {error}\n{splitter}\n'.format(
        splitter='**************************************************', error=error)

    log_file = open(LOG_PATH, 'a+')
    log_file.write(message)
    log_file.close()

    print(message)
