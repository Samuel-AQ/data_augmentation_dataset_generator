from Image_creator import transform_images
from os import mkdir
import Logger


def Main():
    try:
        mkdir('../log')
        Logger.info('Log directory has been created')
    except Exception as e:
        Logger.info('Log system is already up')

    transform_images()


Main()
