import os
from Constants import IMAGE_SIZE
import Logger
import cv2
import albumentations as alb
from tkinter import Tk
from tkinter.filedialog import askdirectory


def get_images():

    root_dataset_directory = choose_directory('Dataset directory')
    root_content = os.listdir(root_dataset_directory)

    Logger.info('Source directory [{}]'.format(root_dataset_directory))
    Logger.info('Found classes ==> {}'.format(root_content))

    images_by_directory = {}

    for current_item in root_content:
        next_is_directory = (os.path.isdir(
            os.path.join(root_dataset_directory, current_item)))
        if (next_is_directory):
            full_class_path = os.path.join(
                root_dataset_directory, current_item)

            images_by_directory[full_class_path] = get_directory_images(
                full_class_path)

    return images_by_directory


def transform_images():

    images_by_class = get_images()

    destination_directory = choose_directory(
        "Augmented dataset directory")

    for class_directory in images_by_class.keys():

        target_directory = os.path.join(destination_directory,
                                        os.path.basename(class_directory))
        try:
            os.mkdir(target_directory)
            Logger.info(
                'Class directory created ===> {}'.format(target_directory))
        except Exception as e:
            Logger.error("Error while creating directory ===>\n{}\n".format(e))

        image_index = 0
        for image in images_by_class[class_directory]:
            original_image = cv2.resize(image, (IMAGE_SIZE, IMAGE_SIZE))

            save_image(target_directory, '{}-{}.jpg'.format(0,
                       image_index), original_image)

            for sample_index in range(1, 3):
                data_augmentation_rules = alb.Compose([
                    alb.RandomCrop(int(IMAGE_SIZE * 0.8),
                                   int(IMAGE_SIZE * 0.8), p=0.7),
                    alb.RandomBrightnessContrast(
                        brightness_limit=(-0.5, 0.5), contrast_limit=(-0.5, 0.5), p=0.6),
                    alb.Rotate(limit=60, p=0.6,
                               border_mode=cv2.BORDER_CONSTANT)
                ])

                augemented_image = cv2.resize(data_augmentation_rules(image=original_image)[
                    'image'], (IMAGE_SIZE, IMAGE_SIZE))

                save_image(
                    target_directory, '{}-{}.jpg'.format(sample_index, image_index), augemented_image)

            image_index += 1

    Logger.info('The augmented dataset has been generated in ===> {}'.format(
        destination_directory))


def save_image(directory, image_name, image_file):

    file_name = os.path.join(directory, image_name)
    cv2.imwrite(file_name, image_file)


def choose_directory(dialog_title):

    root_dialog_window = Tk()

    dialog_response = askdirectory(title=dialog_title)

    # This hides the library default dialog
    root_dialog_window.withdraw()

    if dialog_response != '':
        return dialog_response
    else:
        Logger.error('You need to select a directory')
        quit()


def get_directory_images(directory_path):

    images = []

    for image in os.listdir(directory_path):
        try:
            images.append(cv2.imread(os.path.join(directory_path, image)))
        except Exception as e:
            Logger.error('In image read [{}] ===>\n{}'.format(image, e))

    return images
